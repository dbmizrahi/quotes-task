# Quotes

This project is an example for only demonstrate the skills in Java, Spring and SQL.
You can see the documented API by Swagger2 and deployed through Bitbucket Pipeline to AWS Elastic Beanstalk [here: http://quotes.us-east-2.elasticbeanstalk.com/swagger-ui.html](http://quotes.us-east-2.elasticbeanstalk.com/swagger-ui.html)

## Before run
Setup your MySQL (or MariaDB) credentials in application.properties and pom.xml, create schema "quotes" and then do:

mvn flyway:clean flyway:migrate

## Test
Do:

mvn test -Ptest

## Postman collection execution
Do in Linux or Mac OS.:

sh fill-test-data.sh

(Or Google how to run it in Windows)

Import Quotes.postman_collection.json to your postman and run it as a collection or use 
[Newman](https://learning.getpostman.com/docs/postman/collection_runs/command_line_integration_with_newman/)

### Thanks for your attention