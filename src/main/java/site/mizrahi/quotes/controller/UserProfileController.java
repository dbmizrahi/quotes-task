package site.mizrahi.quotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import site.mizrahi.quotes.entity.QuoteUser;
import site.mizrahi.quotes.service.das.UserService;

@RestController
public class UserProfileController {

    private final UserService userService;

    @Autowired
    public UserProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/api/users/user/{id}",produces = "application/json")
    public QuoteUser getUserDetail(@PathVariable Long id){
        return userService.findById(id);
    }
}
