package site.mizrahi.quotes.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.java.Log;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import site.mizrahi.quotes.dto.QuoteDto;
import org.springframework.core.env.Profiles;
import org.springframework.core.env.Environment;
import site.mizrahi.quotes.service.dai.IQuoteService;

import javax.annotation.PostConstruct;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

@Log //This is the log for test failures only
@RestController
@RequestMapping(produces = "application/json", value = "/quotes")
@Api(basePath = "/dto", tags = "Quotes controller", value = "<h2>David Mizrahi home task from Comm-IT</h2>")
public class QuoteController {

    private final boolean testProfile;
    private final IQuoteService quoteService;

    public QuoteController(IQuoteService quoteService, Environment env) {
        this.testProfile = env.acceptsProfiles(Profiles.of("test"));
        this.quoteService = checkNotNull(quoteService);
    }

    @PostConstruct
    public void postConstruct(){
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @ApiOperation(value = "Add new Quote")
    @PostMapping(value = "")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@securityService.isAdmin()")
    public QuoteDto add(
            @ApiParam(required = true, value = "Quote DTO")
            @RequestBody QuoteDto quote) {
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        return this.quoteService.add(quote);
    }

    @ApiOperation(value = "Get Quote specified by Id")
    @GetMapping(value = "/{quoteId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@securityService.isUserOrAdmin()")
    public QuoteDto get(
            @ApiParam(required = true, value = "Quote Id")
            @PathVariable long quoteId) {
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        return this.quoteService.get(quoteId);
    }

    @ApiOperation(value = "Get Quotes by IDs range")
    @GetMapping(value = "/{start}/{end}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@securityService.isUserOrAdmin()")
    public List<QuoteDto> get(
            @ApiParam(required = true, value = "Start Quote Id")
            @PathVariable long start,
            @ApiParam(required = true, value = "End Quote Id")
            @PathVariable long end) {
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        return this.quoteService.get(start, end);
    }

    @ApiOperation(value = "Update specified Quote with price or items")
    @PutMapping(value = "/{quoteId}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@securityService.isAdmin()")
    public QuoteDto update(
            @ApiParam(required = true, value = "Quote Id")
            @PathVariable long quoteId,
            @ApiParam(required = true, value = "Quote DTO")
            @RequestBody QuoteDto quote) {
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        return this.quoteService.update(quoteId, quote);
    }

    @ApiOperation(value = "Delete specified Quote")
    @DeleteMapping(value = "/{quoteId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@securityService.isAdmin()")
    public void delete(
            @ApiParam(required = true, value = "Quote Id")
            @PathVariable long quoteId) {
        if (this.testProfile)
            log.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        this.quoteService.delete(quoteId);
    }
}
