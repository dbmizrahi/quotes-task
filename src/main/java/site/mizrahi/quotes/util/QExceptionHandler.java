package site.mizrahi.quotes.util;

import lombok.extern.java.Log;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
@Log
public class QExceptionHandler {

    @ExceptionHandler({QException.class})
    @ResponseBody
    @SuppressWarnings("unchecked")
    ResponseEntity handleBadRequest(HttpServletRequest req, QException ex) {
        return new ResponseEntity(new QExceptionInfo(ex), ex.getHttpStatus());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public QExceptionInfo handle(HttpServletRequest req, MethodArgumentNotValidException ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new QExceptionInfo(new QException(ex.getMessage(), HttpStatus.BAD_REQUEST));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public QExceptionInfo handle(HttpServletRequest req, ConstraintViolationException ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new QExceptionInfo(new QException(ex.getMessage(), HttpStatus.BAD_REQUEST));
    }

    @ExceptionHandler({ InvalidDataAccessApiUsageException.class })
    @ResponseBody
    @SuppressWarnings("unchecked")
    ResponseEntity handleBadRequest(HttpServletRequest req, Exception ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new ResponseEntity(
                new QExceptionInfo(new QException("Invalid Request", HttpStatus.BAD_REQUEST)),
                HttpStatus.BAD_REQUEST);
    }
}
