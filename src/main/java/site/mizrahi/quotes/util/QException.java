package site.mizrahi.quotes.util;

import org.springframework.http.HttpStatus;

import javax.ws.rs.BadRequestException;

public class QException extends BadRequestException {

    private String msg;
    private HttpStatus status;

    @SuppressWarnings("WeakerAccess")
    public HttpStatus getHttpStatus() { return this.status; }

    public QException(String error_text, HttpStatus status) {
        super(error_text);
        this.msg = error_text;
        this.status = status;
    }

    public String getMessage(){
        return this.msg;
    }

}
