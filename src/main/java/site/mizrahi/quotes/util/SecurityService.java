package site.mizrahi.quotes.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.mizrahi.quotes.entity.enums.Roles;

import static site.mizrahi.quotes.util.ApiUtil.logFailureMessage;

@Service
public class SecurityService {

    private final ApiUtil apiUtil;

    @Autowired
    public SecurityService(ApiUtil apiUtil) {
        this.apiUtil = apiUtil;
    }

    public boolean isUserOrAdmin() {
        Roles role = this.apiUtil.getUser().getRole();
        return logFailureMessage(role.equals(Roles.USER) ||
                role.equals(Roles.ADMIN),
                "User has not role USER or ADMIN");
    }

    public boolean isAdmin() {
        Roles role = this.apiUtil.getUser().getRole();
        return logFailureMessage(role.equals(Roles.ADMIN),
                "User has not role ADMIN");
    }
}
