package site.mizrahi.quotes.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@NoArgsConstructor
public class QExceptionInfo {
    @JsonIgnore
    LocalDateTime timestamp;
    int errorCode;
    String description;
    String level;
    @JsonIgnore QException e;

    public QExceptionInfo(QException e, HttpStatus status){
        this.timestamp = LocalDateTime.now();
        this.errorCode = status.value();
        this.description = e.getMessage();
        this.level = status.getReasonPhrase();
        this.e = e;
    }

    QExceptionInfo(QException e){
        this.timestamp = LocalDateTime.now();
        this.errorCode = e.getHttpStatus().value();
        this.description = e.getMessage();
        this.level = e.getHttpStatus().getReasonPhrase();
        this.e = e;
    }

    @SuppressWarnings("unchecked")
    public HashMap<String, Object> buildMap(){
        return new ObjectMapper().convertValue(this, HashMap.class);
    }
}
