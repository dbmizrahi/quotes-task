package site.mizrahi.quotes.util;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import site.mizrahi.quotes.entity.QuoteUser;
import site.mizrahi.quotes.repo.UserRepo;

import static java.util.Objects.isNull;

@Service
@Log
public class ApiUtil {

    private final UserRepo userRepo;

    @Autowired
    public ApiUtil(UserRepo repo){
        this.userRepo = repo;
    }

    public QuoteUser getUser(){
        String username = this.getUsername();
        return this.userRepo.findByUsername(username)
                .orElseThrow(()-> new QException("User is not identified", HttpStatus.UNAUTHORIZED));
    }

    //auth - Authorization header containing access token
    public String getUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (isNull(auth)) throw new QException("User is not identified (TOKEN is missing or wrong!)", HttpStatus.UNAUTHORIZED);
        return auth.getName();
    }

    public static boolean logFailureMessage(boolean condition, String failureMessage){
        if (!condition) log.warning(failureMessage);
        return condition;
    }
}
