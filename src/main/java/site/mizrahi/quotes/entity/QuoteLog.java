package site.mizrahi.quotes.entity;

import lombok.*;
import site.mizrahi.quotes.entity.enums.QOperation;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
@Table(name = "quote_log")
public class QuoteLog {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @Column(name = "quote_id")
    private Long quoteId;

    @Enumerated(EnumType.STRING)
    private QOperation operation;

    @Column(name = "error_code")
    private int errorCode;

    private String message;
}
