package site.mizrahi.quotes.entity;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Where(clause = "is_active=1")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
@Table(name = "_quote", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Quote {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    private Double price;

    @Builder.Default
    @Column(name = "is_active")
    private boolean isActive = true;

    @OneToMany(mappedBy = "quote", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Builder.Default
    private List<QuoteItem> items = new ArrayList<>();
}
