package site.mizrahi.quotes.entity;

import lombok.*;
import site.mizrahi.quotes.entity.enums.Roles;

import javax.persistence.*;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
public class QuoteUser {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    @Enumerated(EnumType.STRING)
    private Roles role;
    private String token;
}
