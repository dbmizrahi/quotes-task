package site.mizrahi.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import site.mizrahi.quotes.entity.QuoteLog;

@Repository
public interface QuoteLogRepo extends JpaRepository<QuoteLog, Long> {
}
