package site.mizrahi.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import site.mizrahi.quotes.entity.Quote;

import java.util.List;

@Repository
public interface QuoteRepo extends JpaRepository<Quote, Long> {

    @Query("select max(q.id) from Quote q")
    long findMaxId();

    @Query("select q from Quote q where q.id >= ?1 and q.id < ?2")
    List<Quote> findAllByStartAndEndId(long start, long end);

    boolean existsByName(String name);
}
