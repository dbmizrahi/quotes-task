package site.mizrahi.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import site.mizrahi.quotes.entity.QuoteUser;

import java.util.Optional;

public interface UserRepo extends JpaRepository<QuoteUser, Long> {
    @Query("select u from QuoteUser u where u.username = ?1 and u.password = ?2 ")
    Optional<QuoteUser> login(String username, String password);
    Optional<QuoteUser> findByToken(String token);
    Optional<QuoteUser> findByUsername(String username);
}
