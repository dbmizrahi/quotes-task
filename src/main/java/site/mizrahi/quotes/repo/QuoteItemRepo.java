package site.mizrahi.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import site.mizrahi.quotes.entity.QuoteItem;

@Repository
public interface QuoteItemRepo extends JpaRepository<QuoteItem, Long> {
    void deleteAllByQuoteId(Long quoteId);
}
