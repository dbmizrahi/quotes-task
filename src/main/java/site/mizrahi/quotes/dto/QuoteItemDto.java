package site.mizrahi.quotes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
@ApiModel(description = "Quote item DTO")
public class QuoteItemDto {

    @ApiModelProperty(notes = "Quote item Id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ApiModelProperty(notes = "Quote item name")
    @Size(max = 60, message = "The quote item name must be less than 60 symbols")
    private String name;
}
