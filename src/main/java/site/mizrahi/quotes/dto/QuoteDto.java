package site.mizrahi.quotes.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
@ApiModel(description = "Quote DTO")
@JsonPropertyOrder({ "name", "price", "items" })
public class QuoteDto {

    @ApiModelProperty(notes = "Unique Quote name")
    @NotBlank(message = "The unique quote name must be specified")
    @Size(max = 60, message = "The unique quote name must be less than 60 symbols")
    private String name;

    @ApiModelProperty(notes = "Quote price")
    @Min(value = 0, message = "The value must be positive")
    @NumberFormat
    private Double price;

    @ApiModelProperty(notes = "Quote items")
    @Builder.Default
    private List<QuoteItemDto> items = new ArrayList<>();
}
