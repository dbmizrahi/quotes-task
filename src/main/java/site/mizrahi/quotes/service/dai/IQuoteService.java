package site.mizrahi.quotes.service.dai;

import org.springframework.http.HttpStatus;
import site.mizrahi.quotes.dto.QuoteDto;
import site.mizrahi.quotes.entity.enums.QOperation;

import java.util.List;

public interface IQuoteService {
    QuoteDto add(QuoteDto quote);
    QuoteDto get(long quoteId);
    List<QuoteDto> get(long start, long end);
    QuoteDto update(Long quoteId, QuoteDto quote);
    void delete(long quoteId);
    void saveLogToDb(Long quoteId, QOperation operation, HttpStatus status);
}
