package site.mizrahi.quotes.service.dai;

import site.mizrahi.quotes.entity.QuoteUser;

import java.util.Optional;

public interface IUserService {
    String login(String username, String password);

    Optional findByToken(String token);

    QuoteUser findById(Long id);
}
