package site.mizrahi.quotes.service.dai;

import site.mizrahi.quotes.dto.QuoteDto;
import site.mizrahi.quotes.dto.QuoteItemDto;
import site.mizrahi.quotes.entity.Quote;
import site.mizrahi.quotes.entity.QuoteItem;

import java.util.List;

public interface IQuoteUtilService {
    Quote get(long quoteId);
    QuoteDto getDto(long quoteId);
    QuoteDto convert(Quote quote);
    Quote convert(QuoteDto quoteDto);
    List<QuoteDto> convertQuotes(List<Quote> quotes);
    List<QuoteItemDto> convert(List<QuoteItem> items);
    List<QuoteItem> convertDto(List<QuoteItemDto> items);
    QuoteItem convert(QuoteItemDto itemDto);
    QuoteItemDto convert(QuoteItem item);
}
