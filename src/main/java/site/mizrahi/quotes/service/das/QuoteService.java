package site.mizrahi.quotes.service.das;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.mizrahi.quotes.dto.QuoteDto;
import site.mizrahi.quotes.entity.Quote;
import site.mizrahi.quotes.entity.QuoteItem;
import site.mizrahi.quotes.entity.QuoteLog;
import site.mizrahi.quotes.entity.enums.QOperation;
import site.mizrahi.quotes.repo.QuoteItemRepo;
import site.mizrahi.quotes.repo.QuoteRepo;
import site.mizrahi.quotes.service.dai.IQuoteUtilService;
import site.mizrahi.quotes.service.dai.IQuoteService;
import site.mizrahi.quotes.util.QException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.isNull;

@Service
public class QuoteService implements IQuoteService {

    private final QuoteRepo quoteRepo;
    private final QuoteItemRepo quoteItemRepo;
    private final IQuoteUtilService utilService;
    private final EntityManagerFactory emf;


    private static Logger logger = LoggerFactory.getLogger(QuoteService.class);

    @Autowired
    public QuoteService(QuoteRepo quoteRepo,
                        QuoteItemRepo quoteItemRepo,
                        IQuoteUtilService utilService,
                        EntityManagerFactory emf) {
        this.quoteRepo = checkNotNull(quoteRepo);
        this.utilService = checkNotNull(utilService);
        this.quoteItemRepo = checkNotNull(quoteItemRepo);
        this.emf = emf;
    }

    @Override
    @Transactional
    public QuoteDto add(QuoteDto quoteDto) {
        Quote quote = this.utilService.convert(quoteDto);
        quote.setItems(new ArrayList<>());
        long quoteId;
        try {
            quoteId = this.quoteRepo.save(quote).getId();
        } catch (Exception e) {
            String message = "The Quote with name [" + quoteDto.getName() + "] already exists.";
            HttpStatus status = HttpStatus.BAD_REQUEST;
            saveLogToDb(null, QOperation.CREATE, status);
            throw new QException(message, status);
        }
        updateItems(quoteId, quoteDto);
        saveLogToDb(quoteId, QOperation.CREATE, HttpStatus.CREATED);
        return get(quoteId);
    }

    @Transactional
    void updateItems(long quoteId, QuoteDto quoteDto) {
        String message = "Update operation of the quote with Id [" + quoteId + "] failed";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        Quote quote = this.utilService.get(quoteId);
        this.quoteItemRepo.deleteAllByQuoteId(quoteId);
        List<QuoteItem> items = this.utilService.convertDto(quoteDto.getItems());
        items.forEach(i -> i.setQuote(quote));
        try {
            quote.getItems().clear();
            quote.getItems().addAll(items);
            this.quoteItemRepo.saveAll(items);
        } catch (QException e) {
            saveLogToDb(quoteId, QOperation.UPDATE, status);
            throw new QException(message, status);
        }
        quote.getItems().clear();
        quote.getItems().addAll(items);
        this.quoteItemRepo.saveAll(items);
    }

    @Override
    public QuoteDto get(long quoteId) {
        return this.utilService.getDto(quoteId);
    }

    @Override
    public List<QuoteDto> get(long start, long end) {
        String msg = "Wrong selection parameters";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (start > end && start < this.quoteRepo.findMaxId()) {
            logger.error(msg, new QException(msg, status));
            throw new QException(msg, status);
        }
        return this.utilService.convertQuotes(this.quoteRepo.findAllByStartAndEndId(start, end));
    }

    @Override
    @Transactional
    public QuoteDto update(Long quoteId, QuoteDto quote) {
        if (isNull(quoteId))
            throw new QException("Quote Id must be specified", HttpStatus.BAD_REQUEST);
        String message = "Update operation of the quote with Id [" + quoteId + "] failed";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        Quote quoteEntity;
        if (!this.quoteRepo.existsById(quoteId)) {
            saveLogToDb(quoteId, QOperation.UPDATE, status);
            logger.error(message);
            throw new QException(message, status);
        }
        quoteEntity = this.utilService.get(quoteId);
        if (!isNull(quote.getPrice())) quoteEntity.setPrice(quote.getPrice());
        if (!isNull(quote.getItems())) updateItems(quoteId, quote);
        saveLogToDb(quoteId, QOperation.UPDATE, HttpStatus.OK);
        return get(quoteId);
    }

    @Override
    @Transactional
    public void delete(long quoteId) {
        String message = "Delete operation of the quote with Id [" + quoteId + "] failed";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        try {
            this.utilService.get(quoteId).setActive(false);
        } catch (QException e) {
            saveLogToDb(quoteId, QOperation.DELETE, status);
            logger.error(message);
            throw new QException(message, status);
        }
        saveLogToDb(quoteId, QOperation.DELETE, HttpStatus.NO_CONTENT);
    }


    @Override
    public void saveLogToDb(Long quoteId, QOperation operation, HttpStatus status) {
        QuoteLog log = QuoteLog.builder()
                .createdDate(LocalDateTime.now())
                .errorCode(status.value())
                .message(status.name())
                .operation(operation)
                .quoteId(quoteId)
                .build();
        EntityManager em = emf.createEntityManager();
        EntityTransaction userTransaction = em.getTransaction();
        userTransaction.begin();
        em.persist(log);
        userTransaction.commit();
    }
}
