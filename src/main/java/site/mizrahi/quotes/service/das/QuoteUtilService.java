package site.mizrahi.quotes.service.das;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.mizrahi.quotes.dto.QuoteDto;
import site.mizrahi.quotes.dto.QuoteItemDto;
import site.mizrahi.quotes.entity.Quote;
import site.mizrahi.quotes.entity.QuoteItem;
import site.mizrahi.quotes.repo.QuoteRepo;
import site.mizrahi.quotes.service.dai.IQuoteUtilService;
import site.mizrahi.quotes.util.QException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class QuoteUtilService implements IQuoteUtilService {

    private final QuoteRepo quoteRepo;
    private static Logger logger = LoggerFactory.getLogger(QuoteUtilService.class);

    @Autowired
    public QuoteUtilService(QuoteRepo quoteRepo) {
        this.quoteRepo = checkNotNull(quoteRepo);
    }


    @Override
    @Transactional
    public Quote get(long quoteId) {
        String msg = "The Quote with Id[" + quoteId + "] does not present in database";
        Optional<Quote> quote = this.quoteRepo.findById(quoteId);
        if (!quote.isPresent()) logger.error(msg);
        return quote.orElseThrow(() -> new QException(msg, HttpStatus.NOT_FOUND));
    }

    @Override
    public QuoteDto getDto(long quoteId) {
        return convert(get(quoteId));
    }

    @Override
    public QuoteDto convert(Quote quote) {
        return QuoteDto.builder()
                .name(quote.getName())
                .price(quote.getPrice())
                .items(convert(quote.getItems()))
                .build();
    }

    @Override
    public Quote convert(QuoteDto quoteDto) {
        return Quote.builder()
                .name(quoteDto.getName())
                .price(quoteDto.getPrice())
                .items(convertDto(quoteDto.getItems()))
                .build();
    }

    @Override
    public List<QuoteDto> convertQuotes(List<Quote> quotes) {
        List<QuoteDto> converted = new ArrayList<>(quotes.size());
        quotes.forEach(quote -> converted.add(convert(quote)));
        return converted;
    }

    @Override
    public List<QuoteItemDto> convert(List<QuoteItem> items) {
        List<QuoteItemDto> converted = new ArrayList<>(items.size());
        items.forEach(item -> converted.add(convert(item)));
        return converted;
    }

    @Override
    public List<QuoteItem> convertDto(List<QuoteItemDto> items) {
        List<QuoteItem> converted = new ArrayList<>(items.size());
        items.forEach(item -> converted.add(convert(item)));
        return converted;
    }

    @Override
    public QuoteItem convert(QuoteItemDto itemDto) {
        return QuoteItem.builder()
                .name(itemDto.getName())
                .build();
    }

    @Override
    public QuoteItemDto convert(QuoteItem item) {
        return QuoteItemDto.builder()
                .id(item.getId())
                .name(item.getName())
                .build();
    }
}
