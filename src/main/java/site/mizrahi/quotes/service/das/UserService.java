package site.mizrahi.quotes.service.das;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import site.mizrahi.quotes.entity.QuoteUser;
import site.mizrahi.quotes.repo.UserRepo;
import org.springframework.security.core.userdetails.User;
import site.mizrahi.quotes.security.QuotesPasswordEncoder;
import site.mizrahi.quotes.service.dai.IUserService;
import site.mizrahi.quotes.util.QException;

import java.util.Optional;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class UserService implements IUserService {

    private final UserRepo userRepo;
    private final QuotesPasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepo userRepo,
                       QuotesPasswordEncoder passwordEncoder) {
        this.userRepo = checkNotNull(userRepo);
        this.passwordEncoder = checkNotNull(passwordEncoder);
    }

    @Override
    public String login(String username, String password) {
        QuoteUser user = this.userRepo.findByUsername(username).orElseThrow(()->
                new QException("User not found", HttpStatus.NOT_FOUND));
        String token = UUID.randomUUID().toString();
        if (!passwordEncoder.matches(password, user.getPassword()))
            throw new QException("Password incorrect", HttpStatus.BAD_REQUEST);
        user.setToken(token);
        userRepo.save(user);
        return token;
    }

    @Override
    public Optional findByToken(String token) {
        Optional<QuoteUser> userOptional = userRepo.findByToken(token);
        if(userOptional.isPresent()) {
            QuoteUser quoteUser = userOptional.get();
            User user = new User(quoteUser.getUsername(), quoteUser.getPassword(), true, true, true, true,
                    AuthorityUtils.createAuthorityList(quoteUser.getRole().name()));
            return Optional.of(user);
        }
        return  Optional.empty();
    }

    @Override
    public QuoteUser findById(Long id) {
        Optional<QuoteUser> user = this.userRepo.findById(id);
        return user.orElse(null);
    }
}
