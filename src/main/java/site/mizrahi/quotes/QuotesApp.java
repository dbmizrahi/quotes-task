package site.mizrahi.quotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class QuotesApp {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(QuotesApp.class);
    }
}
