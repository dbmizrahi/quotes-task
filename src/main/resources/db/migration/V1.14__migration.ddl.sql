CREATE TABLE _quote (
    id      BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name 	VARCHAR(60) NOT NULL,
    price   DOUBLE NOT NULL CHECK(price >= 0),
    is_active BOOLEAN DEFAULT TRUE
) ENGINE = InnoDB
  COMMENT 'Main quotes table';

CREATE TABLE item (
    id      BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name 	VARCHAR(60) NOT NULL,
    quote_id BIGINT NOT NULL,
    CONSTRAINT FOREIGN KEY (quote_id) REFERENCES _quote (id)
) ENGINE = InnoDB
  COMMENT 'Main quotes items table';

CREATE TABLE quote_log (
    id              BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    created_date    DATETIME,
    quote_id        BIGINT,
    operation       ENUM('CREATE', 'UPDATE', 'DELETE') NOT NULL,
    error_code      INT,
    message         VARCHAR(60)
) ENGINE = InnoDB
  COMMENT 'Log storage table';

CREATE TABLE user (
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    username        VARCHAR(60) NOT NULL,
    password        VARCHAR(60) NOT NULL,
    role       ENUM('ADMIN', 'USER') NOT NULL,
    token        VARCHAR(60) NOT NULL
) ENGINE = InnoDB
  COMMENT 'Main user table';