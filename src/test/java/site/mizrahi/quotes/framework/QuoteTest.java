package site.mizrahi.quotes.framework;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import site.mizrahi.quotes.QuotesApp;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.MediaType;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { QuotesApp.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@PropertySource("classpath:application.properties")
@AutoConfigureMockMvc
@EnableWebMvc
@Sql(scripts = "/db/test_data.sql")
public abstract class QuoteTest {
    @PersistenceContext
    protected EntityManager em;

    @Autowired
    protected MockMvc mvc;

    protected static ObjectWriter ow = null;
    protected final static ObjectMapper om = new ObjectMapper();

    protected final String userToken = "Bearer 11874d82-6d3a-440c-9cbd-338290b76242";
    protected final String adminToken = "Bearer 295eab36-3b11-4ae0-998d-498c1312b0d2";

    public QuoteTest(){
        SimpleModule module = new SimpleModule();
        om.registerModule(module);
        ow = om.writer().withDefaultPrettyPrinter();
    }

    protected MockHttpServletRequestBuilder setRequestBuilder(MockHttpServletRequestBuilder builder, String token) {
        return builder.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .header("Authorization", token);
    }
}
