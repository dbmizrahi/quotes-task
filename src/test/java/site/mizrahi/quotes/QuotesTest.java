package site.mizrahi.quotes;

import java.util.List;
import java.util.ArrayList;

import site.mizrahi.quotes.dto.QuoteDto;
import site.mizrahi.quotes.repo.QuoteRepo;
import site.mizrahi.quotes.dto.QuoteItemDto;
import site.mizrahi.quotes.framework.QuoteTest;
import site.mizrahi.quotes.service.das.QuoteService;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.test.web.servlet.ResultActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class QuotesTest extends QuoteTest {

    private final QuoteRepo quoteRepo;
    private final QuoteService quoteService;

    @Autowired
    QuotesTest(QuoteRepo quoteRepo,
               QuoteService quoteService) {
        this.quoteRepo = checkNotNull(quoteRepo);
        this.quoteService = checkNotNull(quoteService);
    }

    @Test
    public void testAddQuote() throws Exception {
        // adding a new Quote
        QuoteDto quoteDto = QuoteDto.builder()
                .name("Quote 4")
                .price(32.6)
                .build();
        MockHttpServletRequestBuilder request =
                this.setRequestBuilder(
                        post("/quotes").content(ow.writeValueAsString(quoteDto)),
                        adminToken);
        ResultActions actions = this.mvc.perform(request).andExpect(status().is(201));
        String dto = actions.andReturn().getResponse().getContentAsString();
        String name = om.readValue(dto, QuoteDto.class).getName();
        assertTrue(this.quoteRepo.existsByName(name));

        // adding a Quote with existed name
        QuoteDto wrongNameQuoteDto = QuoteDto.builder()
                .name("Quote 4")
                .price(32.6)
                .build();
        MockHttpServletRequestBuilder badRequest1 =
                this.setRequestBuilder(
                        post("/quotes").content(ow.writeValueAsString(wrongNameQuoteDto)),
                        adminToken);
        this.mvc.perform(badRequest1).andExpect(status().is(400));

        // adding a Quote with negative price
        QuoteDto wrongPriceQuoteDto = QuoteDto.builder()
                .name("Quote 5")
                .price(-32.6)
                .build();
        MockHttpServletRequestBuilder badRequest2 =
                this.setRequestBuilder(
                        post("/quotes").content(ow.writeValueAsString(wrongPriceQuoteDto)),
                        adminToken);
        this.mvc.perform(badRequest2).andExpect(status().is(400));

        // adding a Quote with USER role
        QuoteDto usersQuoteDto = QuoteDto.builder()
                .name("Quote 5")
                .price(32.6)
                .build();
        MockHttpServletRequestBuilder badRequest3 =
                this.setRequestBuilder(
                        post("/quotes").content(ow.writeValueAsString(usersQuoteDto)),
                        userToken);
        this.mvc.perform(badRequest3).andExpect(status().is(403));
    }

    @Test
    public void testGetQuote() throws Exception {
        String expectedName = "Quote 1";
        MockHttpServletRequestBuilder request =
                this.setRequestBuilder(
                        get("/quotes/1"),
                        userToken);
        ResultActions actions = this.mvc.perform(request).andExpect(status().is(200));
        String dto = actions.andReturn().getResponse().getContentAsString();
        String name = om.readValue(dto, QuoteDto.class).getName();
        assertEquals(expectedName, name);
    }

    @Test
    public void testGetQuotes() throws Exception {
        int expectedSize = 3;
        MockHttpServletRequestBuilder request =
                this.setRequestBuilder(
                        get("/quotes/1/5"),
                        userToken);
        ResultActions actions = this.mvc.perform(request).andExpect(status().is(200));
        String dto = actions.andReturn().getResponse().getContentAsString();
        int size = om.readValue(dto, QuoteDto[].class).length;
        assertEquals(expectedSize, size);
    }

    @Test
    public void testUpdateQuote() throws Exception {
        QuoteItemDto item1 = QuoteItemDto.builder()
                .name("Item 10")
                .build();
        QuoteItemDto item2 = QuoteItemDto.builder()
                .name("Item 11")
                .build();
        List<QuoteItemDto> items = new ArrayList<>(2);
        items.add(item1);
        items.add(item2);
        QuoteDto quoteDto = QuoteDto.builder()
                .price(32.6)
                .items(items)
                .build();
        MockHttpServletRequestBuilder request =
                this.setRequestBuilder(
                        put("/quotes/1").content(om.writeValueAsString(quoteDto)),
                        adminToken);
        ResultActions actions = this.mvc.perform(request).andExpect(status().is(200));
        String dto = actions.andReturn().getResponse().getContentAsString();
        int size = om.readValue(dto, QuoteDto.class).getItems().size();
        assertEquals(2, size);
    }

    @Test
    public void testDeleteQuote() throws Exception {
        MockHttpServletRequestBuilder request =
                this.setRequestBuilder(
                        delete("/quotes/3"),
                        adminToken);
        this.mvc.perform(request).andExpect(status().is(204));
        assertEquals(2, this.quoteService.get(1L, 4L).size());
    }
}
