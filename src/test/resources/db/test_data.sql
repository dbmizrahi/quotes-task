# noinspection SqlWithoutWhereForFile

-- cleanup
delete from quotes.quote_log;
delete from quotes.item;
delete from quotes._quote;
delete from quotes.`user`;

-- insert test data
INSERT INTO quotes._quote (id, name, price, is_active) VALUES
(1, 'Quote 1', 11.1, 1),
(2, 'Quote 2', 11.1, 1),
(3, 'Quote 3', 11.1, 1);

INSERT INTO quotes.item (id, name, quote_id) VALUES
(1, 'Item 1', 1),
(2, 'Item 2', 1),
(3, 'Item 3', 2),
(4, 'Item 4', 2),
(5, 'Item 5', 3),
(6, 'Item 6', 3);

INSERT INTO quotes.user (id, username, password, role, token) VALUES
(1, 'user', '$2a$12$8HPShTe4T1NLyXw8Edilt.p84Lmh44XbtQNVUnp.0aoQYcui818dK', 'USER', '11874d82-6d3a-440c-9cbd-338290b76242'), --password
(2, 'admin', '$2a$12$8ZceanHMdpttYW7MXL55AOOlmCaWqxaai7oCoAAfMrGWGSBUO.rI.', 'ADMIN', '295eab36-3b11-4ae0-998d-498c1312b0d2'), --adminpassword
(3, 'testprofile', '$2a$12$s0wdCr6zaayypH7ypugZCeAhm47KoE1HBxbyj8an3YXnGkFDO3lXy', 'USER', ''); --testpassword